<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
   ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string {
        return "Utilisateur : {$this->nom} {$this->prenom} avec comme login {$this->login}";
    }
    public function getLogin(): string {
        return $this->login;
    }
    public function getPrenom(): string {
        return $this->prenom;
    }
    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }
    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }
    public static function recupererUtilisateurs(){
        require_once 'ConnexionBaseDeDonnees.php';
        $instance=ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur');
        $ut=[];
        foreach ($instance->fetchAll() as $table) {
            $utilisateur=new Utilisateur($table['loginBaseDeDonnees'],$table['nomBaseDeDonnees'],$table['prenomBaseDeDonnees']);
            $ut[]=$utilisateur;
        }
        return $ut;
    }
}
?>