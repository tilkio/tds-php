<?php

use App\Covoiturage\Modele\DataObject\Trajet;

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';
require_once 'Trajet.php';

if (!empty($_POST)) {
    $depart = $_POST['depart'];
    $arrivee = $_POST['arrivee'];
    $dateString = $_POST['date'];
    $prix = $_POST['prix'];
    $conducteurLogin = $_POST['conducteurLogin'];
    $nonFumeur = isset($_POST['nonFumeur']) ? 1 : 0;

    echo "<p>Départ : " . htmlspecialchars($depart) . "</p>";
    echo "<p>Arrivée : " . htmlspecialchars($arrivee) . "</p>";
    echo "<p>Date : " . htmlspecialchars($dateString) . "</p>";
    echo "<p>Prix : " . htmlspecialchars($prix) . "</p>";
    echo "<p>Conducteur : " . htmlspecialchars($conducteurLogin) . "</p>";
    echo "<p>Non Fumeur : " . ($nonFumeur ? "Oui" : "Non") . "</p>";

    try {
        $date = new DateTime($dateString);

        $conducteur = Utilisateur::recupererUtilisateurParLogin($conducteurLogin);

        $nouveauTrajet = new Trajet(null, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);

        $nouveauTrajet->ajouter();
        echo "<p>Trajet ajouté avec succès.</p>";

    } catch (PDOException $e) {
        echo "<p>Erreur lors de l'ajout du trajet : " . $e->getMessage() . "</p>";
    } catch (Exception $e) {
        echo "<p>Erreur : " . $e->getMessage() . "</p>";
    }
} else {
    echo "<p>Aucune donnée reçue via le formulaire.</p>";
}
?>