<?php

use App\Covoiturage\Modele\DataObject\Trajet;

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

if (isset($_GET['login']) && isset($_GET['trajet_id'])) {
    $passagerLogin = $_GET['login'];
    $trajetId = $_GET['trajet_id'];

    $trajet = Trajet::recupererTrajetParId($trajetId);

    if ($trajet !== null && $trajet->supprimerPassager($passagerLogin)) {
        echo "Le passager a été désinscrit avec succès.";
    } else {
        echo "Paramètres manquants.";
    }
}
    ?>