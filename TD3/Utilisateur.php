<?php

use App\Covoiturage\Modele\DataObject\Trajet;

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

class Utilisateur
{
    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager = null;

    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login =$login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function afficher(): string
    {
        return "Login: " . $this->login .
            ", Nom: " . $this->nom .
            ", Prénom: " . $this->prenom;
    }

    public static function getUtilisateurs(): array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = 'SELECT * FROM utilisateur';
        $pdoStatement = $pdo->query($sql);

        $utilisateurs = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function recupererUtilisateurParLogin(string $login): ?Utilisateur
    {
        $sql = "SELECT * FROM utilisateur WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );
        $pdoStatement->execute($values);

        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau === false) {
            return null;
        }

        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): bool
    {
        try {

            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = [
                "login" => $this->login,
                "nom" => $this->nom,
                "prenom" => $this->prenom
            ];

            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            if ($e->getCode() == 23000) {
                echo "Erreur : Violation de contrainte d'intégrité (utilisateur existant).";
            } elseif ($e->getCode() == 22001) {
                echo "Erreur : Chaîne de caractères trop longue.";
            } else {
                echo "Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage();
            }
            return false;
        }
    }


    function getTrajetsCommePassager(): array {
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(array $trajets): void {
        $this->trajetsCommePassager = $trajets;
    }

    private function recupererTrajetsCommePassager(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT t.* FROM trajet t
              INNER JOIN passager p ON t.id = p.trajetId
              WHERE p.passagerlogin = :login";
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->execute(['login' => $this->login]);

        $trajets = [];
        while ($row = $pdoStatement->fetch()) {
            $trajets[] = Trajet::construireDepuisTableauSQL($row);
        }

        return $trajets;
    }




}
?>