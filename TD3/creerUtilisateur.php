<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

if (!empty($_POST)) {
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";

    $login = $_POST['login'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];

    echo "<p>Login : " . htmlspecialchars($login) . "</p>";
    echo "<p>Nom : " . htmlspecialchars($nom) . "</p>";
    echo "<p>Prénom : " . htmlspecialchars($prenom) . "</p>";

    try {
        $nouvelUtilisateur = new Utilisateur($login, $nom, $prenom);

        $nouvelUtilisateur->ajouter();

    } catch (PDOException $e) {
        echo "<p>Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage() . "</p>";
    }
} else {
    echo "<p>Aucune donnée reçue via le formulaire.</p>";
}
?>