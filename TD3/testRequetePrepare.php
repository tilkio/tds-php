<?php
require_once 'Utilisateur.php';

$nouvelUtilisateur = new Utilisateur('julient', 'Julien-Servel', 'Thomas');

try {
    $nouvelUtilisateur->ajouter();
    echo "Utilisateur ajouté avec succès.<br>";

    $utilisateurRecupere = Utilisateur::recupererUtilisateurParLogin('julient');

    if ($utilisateurRecupere === null) {
        echo "L'utilisateur n'a pas été trouvé.";
    } else {
        echo $utilisateurRecupere->afficher();
    }
} catch (PDOException $e) {
    echo "Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage();
}
?>