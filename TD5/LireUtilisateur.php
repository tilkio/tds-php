<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

try {
    $utilisateurs = Utilisateur::getUtilisateurs();

    foreach ($utilisateurs as $utilisateur) {
        echo $utilisateur->afficher() . '<br>';
        $trajets = $utilisateur->getTrajetsCommePassager();
        echo "Trajets comme passager:.<br> ";
        if (empty($trajets)) {
            echo "Aucun trajet.<br>";
        } else {
            foreach ($trajets as $trajet) {
                echo $trajet->__toString() . ', ';
            }
            echo "<br>";
        }
    }
} catch (PDOException $e) {
    echo "Erreur lors de la lecture des utilisateurs : " . $e->getMessage();
}
?>