<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = UtilisateurRepository::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('/vueGenerale.php', ['mess'=>"",'utilisateurs' => $utilisateurs, 'titre' => "Liste des utilisateurs", 'cheminCorpsVue' => "utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur=UtilisateurRepository::recupererUtilisateurParLogin($login);

        if($utilisateur==!null){
            self::afficherVue('/vueGenerale.php', ['mess'=>"",'utilisateur' => $utilisateur, 'titre' => "details utilisateur " . $login, 'cheminCorpsVue' => "utilisateur/detail.php"]);
        }
        else{
            self::afficherVue('/vueGenerale.php',['mess'=>"",'titre' => "Detail de l'utilisateur inexistant", 'cheminCorpsVue' => "utilisateur/detail.php"]);
        }

    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('/vueGenerale.php',['mess'=>"",'titre' => "Creer nouveau utilisateur", 'cheminCorpsVue' => "utilisateur/formulaireUtilisateur.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require (__DIR__ . '/../vue' . $cheminVue); // Charge la vue
    }
    public static function creerUtilisateur() : void {
        if (!empty($_GET)) {
            echo "<pre>";
            print_r($_GET);
            echo "</pre>";

            $login = $_GET['login'];
            $nom = $_GET['nom'];
            $prenom = $_GET['prenom'];

            echo "<p>Login : " . htmlspecialchars($login) . "</p>";
            echo "<p>Nom : " . htmlspecialchars($nom) . "</p>";
            echo "<p>Prénom : " . htmlspecialchars($prenom) . "</p>";

            try {
                $nouvelUtilisateur = new utilisateur($login, $nom, $prenom);
                UtilisateurRepository::ajouter($nouvelUtilisateur);
                header('Location: http://localhost/tds-php/TD5/web/controleurFrontal.php?action=utilisateurCree');
                exit();
            } catch (PDOException $e) {
                echo "<p>Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage() . "</p>";
            }
        } else {
            echo "<p>Aucune donnée reçue via le formulaire.</p>";
        }
    }

    public static function utilisateurCree(){
        $utilisateurs = UtilisateurRepository::recupererUtilisateurs();
        self::afficherVue('/vueGenerale.php', ['mess'=>"<p>L'utilisateur a bien été créé !</p>", 'utilisateurs' => $utilisateurs, 'titre' => "Liste des utilisateurs", 'cheminCorpsVue' => "utilisateur/liste.php"]);
    }

    public static function erreur(){
        self::afficherVue('/vueGenerale.php', ['mess=>""','cheminCorpsVue' => "erreur"]);
}
    public static function utilisateurSupprime(){
        $utilisateurs = UtilisateurRepository::recupererUtilisateurs();
        $login = $_GET['login'];
        if(UtilisateurRepository::supprimerParLogin($login)) {
            self::afficherVue('/vueGenerale.php', ['mess' => '<p>utilisateur ' . $login . ' a bien été supprimé</p>', 'utilisateurs' => $utilisateurs, 'titre' => "Liste des utilisateurs", 'cheminCorpsVue' => "utilisateur/liste.php"]);
        }
        else{
            self::erreur();
        }
    }
    public static function formulaireMaj()
    {
        $login = $_GET['login'];
        $utilisateurs=UtilisateurRepository::recupererUtilisateurs();
        self::afficherVue('/vueGenerale.php', ['mess' => '<p>utilisateur ' . $login . ' va être modifier</p>', 'utilisateurs' => $utilisateurs, 'titre' => "Liste des utilisateurs", 'cheminCorpsVue' => "utilisateur/formulaireMiseAJour.php"]);
    }

    public static function utilisateurMiseAJour(){
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateurs=UtilisateurRepository::recupererUtilisateurs();
        if(UtilisateurRepository::MiseAJourParLogin($login, $nom, $prenom)) {
            self::afficherVue('/vueGenerale.php', ['mess' => '<p>utilisateur ' . $login . ' a bien été smodifié</p>', 'utilisateurs' => $utilisateurs, 'titre' => "Liste des utilisateurs", 'cheminCorpsVue' => "utilisateur/liste.php"]);
        }
        else{
            self::erreur();
        }

    }
}
?>