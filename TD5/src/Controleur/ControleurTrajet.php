<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\TrajetRepository;

class ControleurTrajet{

    public static function afficherListe()
    {
        $Trajets = TrajetRepository::recupererTrajets(); //appel au modèle pour gérer la BD
        self::afficherVue('/vueGenerale.php', ['mess'=>"",'Trajets' => $Trajets, 'titre' => "Liste des Trajets", 'cheminCorpsVue' => "Trajet/liste.php"]);

    }

    public static function erreur(){
        self::afficherVue('/vueGenerale.php', ['mess'=>"",'Trajets' => "", 'titre' => "Erreur", 'cheminCorpsVue' => "Trajet/erreur.php"]);

    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require (__DIR__ . '/../vue' . $cheminVue); // Charge la vue
    }
}
