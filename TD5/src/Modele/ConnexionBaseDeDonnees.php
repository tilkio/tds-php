<?php
namespace App\Covoiturage\Modele;
use App\Covoiturage\Configuration\ConfigurationBaseDeDonnees;
use \PDO as PDO;
class ConnexionBaseDeDonnees {
    private static ?ConnexionBaseDeDonnees $instance = null;

    private PDO $pdo;

    private function __construct() {
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $motDePasse = ConfigurationBaseDeDonnees::getMotDePasse();

        $this->pdo = new PDO(
            "mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees",
            $login,
            $motDePasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private static function getInstance(): ConnexionBaseDeDonnees
    {
        if (is_null(self::$instance)) {
            self::$instance = new ConnexionBaseDeDonnees();
        }
        return self::$instance;
    }

    public static function getPdo(): PDO {
        return self::getInstance()->pdo;
    }
}
?>
