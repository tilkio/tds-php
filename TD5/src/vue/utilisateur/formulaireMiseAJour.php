<?php
// Définir des valeurs par défaut pour login, nom et prénom
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
$login = $_GET['login'];
$nom = UtilisateurRepository::recupererUtilisateurParLogin($login)->getNom();
$prenom = UtilisateurRepository::recupererUtilisateurParLogin($login)->getprenom();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mise a Jour Utilisateur</title>
</head>
<body>

<form method="get" action="../web/controleurFrontal.php">
    <input type='hidden' name='action' value='utilisateurMiseAJour'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" name="login" id="login_id" value="<?= htmlspecialchars($login) ?>" readonly>
        </p>

        <p class="InputAddOn">
            <label class="inputAddon-item" for="nom_id">Nom&#42;</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?= htmlspecialchars($nom) ?>" name="nom" id="nom_id" required />
        </p>
        <p class="InputAddOn">
            <label class="inputAddon-item" for="prenom_id">Prénom&#42;</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?= htmlspecialchars($prenom) ?>" name="prenom" id="prenom_id" required />
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>
