<?php
/** @var Trajet[] $Trajets */
foreach ($Trajets as $trajet) {
    echo "<p> Départ: " . htmlspecialchars($trajet->getDepart()) . " Arrivée: " . htmlspecialchars($trajet->getArrivee()) ." Date: " . $trajet->getDate()->format('d/m/Y H:i') . "<p>Prix: " . $trajet->getPrix() . " € Conducteur: " . htmlspecialchars($trajet->getConducteur()->getNom()) . " Non Fumeur: " . ($trajet->isNonFumeur() ? 'Oui' : 'Non') . "</p>";
}
?>