<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../../ressource/Style.css">
    <meta charset="UTF-8">
    <title><?php echo/**
         * @var string $titre
         */ $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $mess
     */
    echo $mess;
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Igritt
    </p>
</footer>
</body>
</html>