<?php
require_once(__DIR__ . '/../src/Lib/Psr4AutoloaderClass.php');
use App\Covoiturage\Controleur\ControleurUtilisateur;
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');
// On récupère l'action passée dans l'URL
$action = isset($_GET['action']) ? $_GET['action'] : 'afficherListe';
$controleur = isset($_GET['controleur']) ? $_GET['controleur'] : 'Utilisateur';
// Appel de la méthode statique $action de ControleurUtilisateur
$nomDeClasseControleur = 'App\\Covoiturage\\Controleur\\Controleur' . ucfirst($controleur);
if(class_exists($nomDeClasseControleur) && method_exists($nomDeClasseControleur, $action)) {
    $nomDeClasseControleur::$action();
} else {
    $nomDeClasseControleur::erreur();
}

?>