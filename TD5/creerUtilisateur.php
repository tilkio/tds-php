<?php
use App\Covoiturage\Modele\utilisateur;
if (!empty($_GET)) {
    echo "<pre>";
    print_r($_GET);
    echo "</pre>";

    $login = $_GET['login'];
    $nom = $_GET['nom'];
    $prenom = $_GET['prenom'];

    echo "<p>Login : " . htmlspecialchars($login) . "</p>";
    echo "<p>Nom : " . htmlspecialchars($nom) . "</p>";
    echo "<p>Prénom : " . htmlspecialchars($prenom) . "</p>";

    try {
        $nouvelUtilisateur = new utilisateur($login, $nom, $prenom);
        $nouvelUtilisateur->ajouter();


        header('Location: http://localhost/tds-php/TD5/web/controleurFrontal.php');
        exit();
    } catch (PDOException $e) {
        echo "<p>Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage() . "</p>";
    }
} else {
    echo "<p>Aucune donnée reçue via le formulaire.</p>";
}
?>
