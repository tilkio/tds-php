<?php

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurUtilisateur;



// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');




// On récupère l'action passée dans l'URL
if (isset($_GET['action']) && isset($_GET['controleur'])) {
    $action = $_GET['action'];
    $controleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($_GET['controleur']);

    if (class_exists($controleur)){
        if (in_array($action, get_class_methods($controleur))){
            // Appel de la méthode statique $action de ControleurUtilisateur
            $controleur::$action();
        }else{
            $controleur::afficherErreur("l'action ne correspond a aucune des fonctions");
        }
    }else{
        ControleurUtilisateur::afficherErreur("la classe n'existe pas <br>" . $controleur);
    }

}else{
    ControleurUtilisateur::afficherListe();
}

?>