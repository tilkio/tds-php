<?php

use App\Covoiturage\Modele\DataObject\Trajet;

require 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';
require_once 'Trajet.php';

try {
    $trajets = Trajet::recupererTrajets();

    foreach ($trajets as $trajet) {
        echo $trajet->__toString() . '<br>';
        $passagers = $trajet->getPassagers();
        echo "Passagers: ";
        if (empty($passagers)) {
            echo "Aucun pass.<>";
        } {
            foreach ($passagers as $passager) {
                echo $passager->getPrenom() . ' ' . $passager->getNom();
                echo ' <a href="supprimerPassager.php?login=' . urlencode($passager->getLogin()) . '&trajet_id=' .($trajet->getId()) . '">Désinscrire</a>, ';
            }
            echo "<br>";
        }
    }
} catch (Exception $e) {
    echo "Erreur lors de la lecture des trajets : " . $e->getMessage();
}
?>