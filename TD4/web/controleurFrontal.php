<?php
require_once 'ControleurUtilisateur.php';
// On récupère l'action passée dans l'URL
$action = isset($_GET['action']) ? $_GET['action'] : 'afficherListe';
// Appel de la méthode statique $action de ControleurUtilisateur
ControleurUtilisateur::$action();
?>