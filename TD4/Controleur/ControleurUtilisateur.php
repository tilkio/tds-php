<?php
require_once ('../Modele/modeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = modeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('/utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }
    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur=modeleUtilisateur::recupererUtilisateurParLogin($login);

        if($utilisateur==!null){
            self::afficherVue('/utilisateur/detail.php', ['utilisateur'=>$utilisateur]);
        }
        else{
            self::afficherVue('/utilisateur/erreur.php');
        }

    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('utilisateur/formulaireUtilisateur.php');
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
}
?>