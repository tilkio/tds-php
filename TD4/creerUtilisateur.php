<?php
require_once 'Modele/ConnexionBaseDeDonnees.php';
require_once 'Modele/modeleUtilisateur.php';

if (!empty($_GET)) {
    echo "<pre>";
    print_r($_GET);
    echo "</pre>";

    $login = $_GET['login'];
    $nom = $_GET['nom'];
    $prenom = $_GET['prenom'];

    echo "<p>Login : " . htmlspecialchars($login) . "</p>";
    echo "<p>Nom : " . htmlspecialchars($nom) . "</p>";
    echo "<p>Prénom : " . htmlspecialchars($prenom) . "</p>";

    try {
        $nouvelUtilisateur = new modeleUtilisateur($login, $nom, $prenom);
        $nouvelUtilisateur->ajouter();

        // Redirection vers root.php après l'ajout
        header('Location: http://localhost/tds-php/TD4/Controleur/root.php');
        exit(); // S'assurer que le script s'arrête après la redirection
    } catch (PDOException $e) {
        echo "<p>Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage() . "</p>";
    }
} else {
    echo "<p>Aucune donnée reçue via le formulaire.</p>";
}
?>
